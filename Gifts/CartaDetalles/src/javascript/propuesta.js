let timer = document.getElementById("timer");
const init = Date.parse("Jun 25, 2016");

setInterval(() => {
  timer.innerText = this.formatDate(Date.now() - init);
}, 27);

function formatDate(date) {
  const splited = date.toString().split("").reverse();
  const counter = [];
  
  splited.forEach((element, index) => {
    if (index % 3 === 0 && index !== 0) {
      element = `${element}.`;
    }

    counter.unshift(element);
  });

  return counter.join("");
}
